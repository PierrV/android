package com.example.tennistracker;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;

import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.provider.MediaStore;
import android.provider.Settings;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.Locale;

public class MainActivity extends AppCompatActivity implements LocationListener {

    int EquipeAscore=0;
    int EquipeBscore=0;

    int FauteEquipeA=0;
    int RougeEquipeA=0;
    int JauneEquipeA=0;

    int FauteEquipeB=0;
    int RougeEquipeB=0;
    int JauneEquipeB=0;

    DatabaseHelper myDB;
    Button btnNext;
    Button butA;
    Button butCSCA;
    Button butB;
    Button butCSCB;
    ImageButton butPhoto;

    private static final int PERMISSION_CODE = 101;
    TextView locationText;
    Button getLocation;
    String[] permissions_all={Manifest.permission.ACCESS_COARSE_LOCATION,Manifest.permission.ACCESS_FINE_LOCATION};
    LocationManager locationManager;
    boolean isGpsLocation;
    Location loc;
    boolean isNetworklocation;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadLocale();
        setContentView(R.layout.activity_main);
        progressDialog=new ProgressDialog(MainActivity.this);
        progressDialog.setMessage("Fetching location...");

        locationText=findViewById(R.id.location);
        getLocation=findViewById(R.id.getlocation);

        getLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.show();
                getLocation();
            }
        });



        Button changeLang = (Button) findViewById(R.id.buttonChangeLang) ;
        changeLang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showChangeLanguageDialog();
            }
        });


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        butA = (Button) findViewById(R.id.button7);
        butCSCA = (Button) findViewById(R.id.button9);
        butB = (Button) findViewById(R.id.button11);
        butCSCA = (Button) findViewById(R.id.button9);
        butCSCB = (Button) findViewById(R.id.button13);
        myDB = new DatabaseHelper(this);

        butPhoto = (ImageButton)findViewById(R.id.buttonPhoto);
        butPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dispatchTakePictureIntent();
            }
        });

        btnNext = (Button)findViewById(R.id.buttonNext);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Main2Activity.class);
                AddData(EquipeAscore, EquipeBscore);


                startActivity(intent);
            }
        });
    }

    static final int REQUEST_IMAGE_CAPTURE = 1;

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }
    //choisir laes langue dispo dans la config

    private void showChangeLanguageDialog() {
        final String[] listItems = {"Francais", "English"};
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(MainActivity.this);
        mBuilder.setTitle("Choose Language...");
        mBuilder.setSingleChoiceItems(listItems, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(which ==0){
                    setLocale("fr");
                    recreate();
                }
                else if (which == 1){
                    setLocale("en");
                    recreate();
                }
                dialog.dismiss();

            }
        });
        AlertDialog mDialog = mBuilder.create();
        mDialog.show();
    }
//défini la langue dans les parametres
    private void setLocale(String lang) {
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());

        SharedPreferences.Editor editor = getSharedPreferences("Settings", MODE_PRIVATE).edit();
        editor.putString("my_Lang", lang);
        editor.apply(); //*applique les nouvelles config, preference
    }
//charger la langue parametré
    public void loadLocale(){
        SharedPreferences prefs = getSharedPreferences("Settings", Activity.MODE_PRIVATE);
        String language = prefs.getString("my_Lang", "");
        setLocale(language);
    }

    public void AddData(int EquipeAscore, int EquipeBscore){
        boolean insertData = myDB.addData(EquipeAscore, EquipeBscore);

        if (insertData == true){
            Toast.makeText(MainActivity.this,"Successfully entered data",Toast.LENGTH_LONG).show();
        }else {
            Toast.makeText(MainActivity.this,"Something went wrong",Toast.LENGTH_LONG).show();

        }
    }

   /* @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void butEquipeA(View view) {
        EquipeAscore = EquipeAscore+1;
        display(EquipeAscore);
    }

    private void display(int equipeAscore) {
        TextView EquipeAvalue = (TextView)findViewById(R.id.EquipeAvalue);
        EquipeAvalue.setText(""+ equipeAscore);
    }

    public void butEquipeB(View view) {
        EquipeBscore = EquipeBscore+1;
        displayB(EquipeBscore);
    }

    private void displayB(int equipeBscore) {
        TextView EquipeBvalue = (TextView)findViewById(R.id.EquipeBvalue);
        EquipeBvalue.setText(""+ equipeBscore);
    }

    public void reset(View view) {
        EquipeAscore=0;
        EquipeBscore=0;

        FauteEquipeA=0;
        RougeEquipeA=0;
        JauneEquipeA=0;

        FauteEquipeB=0;
        RougeEquipeB=0;
        JauneEquipeB=0;
        display(EquipeAscore);
        displayB(EquipeBscore);

        displayC(FauteEquipeA);
        displayD(RougeEquipeA);
        displayE(JauneEquipeA);

        displayF(FauteEquipeB);
        displayG(RougeEquipeB);
        displayH(JauneEquipeB);
    }

    public void fauteEquipeA(View view) {
        FauteEquipeA=FauteEquipeA+1;
        displayC(FauteEquipeA);
    }

    private void displayC(int fauteEquipeA) {
        TextView fauteEquipeAvalue = (TextView)findViewById(R.id.fauteEquipeAvalue);
        fauteEquipeAvalue.setText(""+fauteEquipeA);
    }

    public void rougeEquipeA(View view) {
        RougeEquipeA=RougeEquipeA+1;
        displayD(RougeEquipeA);
    }

    private void displayD(int rougeEquipeA) {
        TextView rougeEquipeAvalue = (TextView)findViewById(R.id.rougeEquipeAvalue);
        rougeEquipeAvalue.setText(""+rougeEquipeA);
    }

    public void jauneEquipeA(View view) {
        JauneEquipeA=JauneEquipeA+1;
        displayE(JauneEquipeA);
    }

    private void displayE(int jauneEquipeA) {
        TextView jauneEquipeAvalue = (TextView)findViewById(R.id.jauneEquipeAvalue);
        jauneEquipeAvalue.setText(""+jauneEquipeA);
    }

    public void fauteEquipeB(View view) {
        FauteEquipeB=FauteEquipeB+1;
        displayF(FauteEquipeB);
    }

    private void displayF(int fauteEquipeB) {
        TextView fauteEquipeBvalue = (TextView)findViewById(R.id.fauteEquipeBvalue);
        fauteEquipeBvalue.setText(""+fauteEquipeB);
    }

    public void rougeEquipeB(View view) {
        RougeEquipeB=RougeEquipeB+1;
        displayG(RougeEquipeB);
    }

    private void displayG(int rougeEquipeB) {
        TextView rougeEquipeBvalue = (TextView)findViewById(R.id.rougeEquipeBvalue);
        rougeEquipeBvalue.setText(""+rougeEquipeB);
    }

    public void jauneEquipeB(View view) {
        JauneEquipeB=JauneEquipeB+1;
        displayH(JauneEquipeB);
    }

    private void displayH(int jauneEquipeB) {
        TextView jauneEquipeBvalue = (TextView)findViewById(R.id.jauneEquipeBvalue);
        jauneEquipeBvalue.setText(""+jauneEquipeB);
    }

    private void getLocation(){
        if (Build.VERSION.SDK_INT>=23){
            if (checkPermission()){
                getDeviceLocation();
            }
            else {
                requestPermission();
            }
        }
        else {
            getDeviceLocation();
        }
    }

    private void requestPermission(){
        ActivityCompat.requestPermissions(MainActivity.this, permissions_all, PERMISSION_CODE);
    }

    private boolean checkPermission(){
        for (int i=0;i<permissions_all.length;i++){
            int result = ContextCompat.checkSelfPermission(MainActivity.this, permissions_all[i]);
            if (result == PackageManager.PERMISSION_GRANTED){
                continue;
            }
            else {
                return false;
            }
        }
        return true;
    }

    private void getDeviceLocation(){
        locationManager=(LocationManager)getSystemService(Service.LOCATION_SERVICE);
        isGpsLocation = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        isNetworklocation = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        if (!isGpsLocation && !isNetworklocation){
            showSettingsForLocation();
            getLastlocation();
        }
        else {
            getFinalLocation();
        }
    }

    private void getLastlocation() {
        if(locationManager != null) {
            try {
                Criteria criteria = new Criteria();
                String provider = locationManager.getBestProvider(criteria, false);
                Location location = locationManager.getLastKnownLocation(provider);
            } catch (SecurityException e) {
                e.printStackTrace();
            }
        }

    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case PERMISSION_CODE:
                if (grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    getFinalLocation();
                }
                else {
                    Toast.makeText(this, "Permission Failed", Toast.LENGTH_SHORT).show();
                }
        }

    }

    private void getFinalLocation(){
        try {
            if (isGpsLocation) {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000*60*1, 10, MainActivity.this);
                if (locationManager != null) {
                    loc=locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                    if (loc!=null){
                        updateUI(loc);
                    }
                }
            }
            else if (isNetworklocation) {
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000*60*1, 10, MainActivity.this);
                if (locationManager != null) {
                    loc=locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                    if (loc!=null){
                        updateUI(loc);
                    }
                }
            }
            else{
                Toast.makeText(this, "Can't Get Location", Toast.LENGTH_SHORT).show();
            }

        } catch (SecurityException e) {
            Toast.makeText(this, "Can't Get Location", Toast.LENGTH_SHORT).show();
        }
    }

    private void updateUI(Location loc) {
        if (loc.getLatitude() == 0 && loc.getLongitude() == 0){
            getDeviceLocation();
        }
        else {
            progressDialog.dismiss();
            locationText.setText("Location : " + loc.getLatitude() + " , " + loc.getLongitude());
        }
    }

    private void showSettingsForLocation(){
        AlertDialog.Builder al = new AlertDialog.Builder(MainActivity.this);
        al.setTitle("Location Not Enabled!");
        al.setMessage("Enable Location ?");
        al.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent (Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        });
        al.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        al.show();
    }

    @Override
    public void onLocationChanged(Location location) {
        updateUI(location);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
